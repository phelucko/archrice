#!/bin/sh

SRC_DIR=$(realpath $(dirname $0))

# system files
[ -d "$SRC_DIR/system/common" ] && cp -R "$SRC_DIR"/system/common/* /
[ -d "$SRC_DIR/system/$(hostname)" ] && cp -R "$SRC_DIR/system/$(hostname)"/* /

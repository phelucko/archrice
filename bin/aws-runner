#!/bin/sh

# This is a runner that executes the script on multiple AWS profiles

if [ $# -eq 0 ]; then
  echo "Usage: aws-runner -p <profile_pattern> <script.sh>"
  exit
fi

POSITIONAL_ARGS=()

while [[ $# -gt 0 ]]; do
  case $1 in
    -p|--profile-pattern)
      PROFILE_PATTERN="$2"
      shift # past argument
      shift # past value
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
      ;;
    *)
      POSITIONAL_ARGS+=("$1") # save positional arg
      shift # past argument
      ;;
  esac
done

profiles="$(cat "$HOME/.aws/config" | awk '$1 == "[profile" {print substr($2, 1, length($2)-1)}' | grep $PROFILE_PATTERN)"

set -- "${POSITIONAL_ARGS[@]}" # restore positional parameters

for profile in $profiles
do
  echo "Running $1 with profile $profile..."
  aws-profiler $profile
  source "$XDG_CACHE_HOME/.aws_env"
  source "$1"
done

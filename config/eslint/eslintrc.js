const m = require('module');

// global plugin resolution hack
// https://github.com/eslint/eslint/issues/11914
const hacks = [
  'eslint-config-airbnb',
  'eslint-plugin-import',
  'eslint-plugin-jsx-a11y',
  'eslint-plugin-react',
  'eslint-plugin-react-hooks',
];

/* eslint-disable no-underscore-dangle */
const moduleFindPath = m._findPath;
m._findPath = (request, paths, isMain) => {
  const r = moduleFindPath(request, paths, isMain);
  if (!r && hacks.includes(request)) {
    // use "npm prefix -g" to determine the global package directory
    return require.resolve(`${process.env.HOME}/.local/share/npm-global/lib/node_modules/${request}`);
  }
  return r;
};

module.exports = {
  root: true,
  extends: 'airbnb',
  env: {
    browser: true,
    es6: true,
    node: true,
    jest: true,
  },
  parserOptions: {
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  rules: {
    // jsx file extension
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],

    // suppress errors for missing 'import React' in files
    'react/react-in-jsx-scope': 'off',

    // allow the use of console
    'no-console': [0],

    // disallow undeclared variables
    'no-undef': 'error',

    // use Stroustrup brace style
    'brace-style': [2, 'stroustrup', { allowSingleLine: true }],

    // always add space in object
    'object-curly-spacing': ['error', 'always'],

    // arrow function parens
    'arrow-parens': ['error', 'as-needed'],
  },
};

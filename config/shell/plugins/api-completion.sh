#!/usr/bin/sh

#
# Completion for api:
#
_api_completions()
{
  API_HOME="$XDG_DATA_HOME/api"
  cur="${COMP_WORDS[COMP_CWORD]}"
  prev="${COMP_WORDS[COMP_CWORD-1]}"

  # parse arguments
  set -- "${COMP_WORDS[@]}"

  shift # always skip the first item because it's the name of the command

  args=()
  while [[ $# -gt 0 ]]
  do
    key="$1"

    case $key in
      -v|--verbose)
        shift # past argument
        ;;
      -f|--format)
        shift # past argument
        ;;
      -p|--payload)
        payload=$2
        shift # past argument
        shift # past value
        ;;
      *)
        # arguments
        args+=("$1") # save it in an array for later
        shift # past argument
        ;;
    esac
  done

  if [ ${#args[@]} -ge 1 ]; then
    HOST_FILE="$API_HOME/${args[0]}"
    PROJECT_HOME="$(dirname $HOST_FILE)"
  fi

  case "$prev" in
		-p|--payload)
      # complete payload files
      if [ ${#args[@]} -eq 1 ]; then
        COMPREPLY=($(compgen -W "$(find -L $PROJECT_HOME -type f -path '*/payloads/*' -printf '%f\n')" -- "$cur"))
      else
        COMPREPLY=($(compgen -W "$(find -L $API_HOME -type f -path '*/payloads/*' -printf '%f\n')" -- "$cur"))
      fi
			;;

		*)
      case ${#args[@]} in
        1)
          # complete host entry
          COMPREPLY=($(compgen -W "$(find -L $API_HOME -maxdepth 2 -type f ! -name endpoints -printf '%P\n')" -- "$cur"))
          ;;

        2)
          # complete endpoint entry
          ENDPOINT_FILE="$PROJECT_HOME/endpoints"

          if [ -f "$ENDPOINT_FILE"  ]; then
            COMPREPLY=($(compgen -W "$(awk '{print $1}' $ENDPOINT_FILE)" -- "$cur"))
          fi
          ;;

        *)
          ;;
      esac
			;;
  esac
}

complete -F _api_completions api

#######################################################################
# FZF Settings
#######################################################################

export FZF_DEFAULT_OPTS="
--layout=reverse
--height=80%
--preview='([[ -f {} ]] && (bat --style=numbers --color=always {} || cat {})) || ([[ -d {} ]] && (tree -C {} | less)) || echo {} 2> /dev/null | head -200'
--preview-window=hidden
--color='hl:148,hl+:154,pointer:032,marker:010,bg+:237,gutter:008'
--bind='?:toggle-preview'
--bind='ctrl-a:select-all'
--bind='ctrl-f:page-down'
--bind='ctrl-b:page-up'
--bind='ctrl-y:execute-silent(echo {} | xclip -selection clipboard)+abort'
"
# use rg to get a list of files in current directory
export FZF_DEFAULT_COMMAND='rg --hidden --ignore-file ~/.config/ripgrep/rgignore --files'
# use fd to get a list of files and folders in home directory
export FZF_CTRL_T_COMMAND='fd --hidden --ignore-file ~/.config/ripgrep/rgignore . $SRC_HOME ~'
export FZF_CTRL_T_OPTS="--select-1 --exit-0"
export FZF_CTRL_R_OPTS="--preview 'echo {}' --preview-window down:3:hidden:wrap --bind '?:toggle-preview'"
if [[ $SHELL =~ "zsh$" ]]; then
  source /usr/share/fzf/completion.zsh
  source /usr/share/fzf/key-bindings.zsh
else
  source /usr/share/fzf/completion.bash
  source /usr/share/fzf/key-bindings.bash
fi

### Quick Jump Settings ###

# change to any directory with fzf
z() {
  _fzf_dir "cd" $HOME,$SRC_HOME $1
}

# change to sub directory with fzf
zz() {
  _fzf_dir "cd" . $1
}

# display any directory with fzf
ztr() {
  _fzf_dir "tree" $HOME,$SRC_HOME $1
}

# fzf shortcut functions
fb() {
  _fzf_file "bat"  "$1"
}

fl() {
  _fzf_file "less" "$1"
}

fv() {
  _fzf_file "$EDITOR" "$1"
}

fo() {
  _fzf_file "xdg-open" "$1"
}

fmpv() {
  _fzf_file "mpv" "$1"
}

# find-in-file - usage: fif <SEARCH_TERM>
fif() {
  if [ ! "$#" -gt 0 ]; then
    echo "Need a string to search for!";
    return 1;
  fi

  local result=$(rg --hidden --ignore-file ~/.config/ripgrep/rgignore --files-with-matches --no-messages "$1" | fzf $FZF_PREVIEW_WINDOW --preview "rg --ignore-case --pretty --context 10 '$1' {}")
  if [ ! -z "$result" ]; then
    less "$result"
  fi
}

#######################################################################
# FZF Helper Functions
#######################################################################

# execute command on file with fzf
# usage: _fzf_file [arg1: command] [arg2: search-term]
_fzf_file() {
  local search_term="$2"
  if [ ! -z "$search_term" ]; then
    search_term="-q $search_term"
  fi

  local result=$(fzf $search_term)
  if [ ! -z "$result" ]; then
    $1 "$result"
  fi
}

# execute command on directory with fzf
# usage: _fzf_dir [arg1: command] [arg2: search-dirs] [arg3: search-term]
_fzf_dir() {
  local search_term="$3"
  if [ ! -z "$search_term" ]; then
    search_term="-q $search_term"
  fi

  local dest_dir=$(fd --type d --follow --ignore-file ~/.config/ripgrep/rgignore . $(echo $2 | tr ',' ' ') | fzf $search_term)
  if [ ! -z "$dest_dir" ]; then
    $1 "$dest_dir"
  fi
}


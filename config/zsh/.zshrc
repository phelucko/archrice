# Enable colors and change prompt:
autoload -U colors && colors	# Load colors
my_prompt() {
  # get current selected aws profile
  # env cache is set by dmenuaws
  aws_env_cache="$XDG_CACHE_HOME/.aws_env"
  if [ -f "$aws_env_cache" ]; then
    source "$aws_env_cache"
    aws_info="%{$fg[yellow]%}$AWS_PROFILE "
  else
    unset AWS_PROFILE
    aws_info=""
  fi

  export PS1="%B%{$fg[magenta]%}[${aws_info}%{$fg[cyan]%}%1~%{$fg[magenta]%}]%{$reset_color%}$%b "
  # export PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M${aws_info} %{$fg[magenta]%}%1~%{$fg[red]%}]%{$reset_color%}$%b "
}
precmd_functions+=( my_prompt )
setopt autocd		# Automatically cd into typed directory.
stty stop undef		# Disable ctrl-s to freeze terminal.

# History in cache directory:
setopt share_history
HISTSIZE=10000
SAVEHIST=10000
HISTFILE="$XDG_CACHE_HOME/zsh/history"

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

# Bash completion compatibility mode
autoload bashcompinit
bashcompinit

# vi mode
bindkey -v
export KEYTIMEOUT=1

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

bindkey -s '^f' 'cd "$(dirname "$(fzf)")"\n'

bindkey '^[[P' delete-char

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

# powerline
if [ -f "/usr/share/powerline/bindings/zsh/powerline.zsh" ]; then
  powerline-daemon -q
  . /usr/share/powerline/bindings/zsh/powerline.zsh
fi

# Load syntax highlighting; should be last.
# This requires the AUR zsh-fast-syntax-highlighting package
[ -f "/usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh" ] \
  && source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh 2>/dev/null

# Extra aliases and shortcuts if existent.
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/shortcuts" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/shortcuts"
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliases" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliases"
if grep --quiet "^enable-ssh-support$" $GNUPGHOME/gpg-agent.conf 2>/dev/null; then
  [ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/gpg-ssh-support" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/gpg-ssh-support"
fi
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/workrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/workrc"

#####################################
# Extra plugins
#####################################

# api-completion script
if [ -f $XDG_CONFIG_HOME/shell/plugins/api-completion.sh ]; then
  source $XDG_CONFIG_HOME/shell/plugins/api-completion.sh
fi

# fzf config
if [ -f $XDG_CONFIG_HOME/shell/plugins/fzf-config.sh ]; then
  source $XDG_CONFIG_HOME/shell/plugins/fzf-config.sh
fi

# forgit plugin
if [ -f $XDG_CONFIG_HOME/shell/plugins/forgit.plugin.sh ]; then
  source $XDG_CONFIG_HOME/shell/plugins/forgit.plugin.sh
fi

# extra functions
if [ -f $XDG_CONFIG_HOME/shell/plugins/extra-functions.sh ]; then
  source $XDG_CONFIG_HOME/shell/plugins/extra-functions.sh
fi

# java development setup
export SDKMAN_DIR="$XDG_DATA_HOME/sdkman"
[ -d "$SDKMAN_DIR/candidates/java/current" ] && export JAVA_HOME="$SDKMAN_DIR/candidates/java/current"
[ -d "$SDKMAN_DIR/candidates/groovy/current" ] && export GROOVY_HOME="$SDKMAN_DIR/candidates/groovy/current"
[ -d "$SDKMAN_DIR/candidates/grails/current" ] && export GRAILS_HOME="$SDKMAN_DIR/candidates/grails/current"
[ -d "$SDKMAN_DIR/candidates/gradle/current" ] && export GRADLE_HOME="$SDKMAN_DIR/candidates/gradle/current"

# nvm setup
export NVM_DIR="$XDG_CONFIG_HOME/nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
if [ -s "$NVM_DIR/nvm.sh" ]; then
  autoload -U add-zsh-hook

  load-nvmrc() {
    local nvmrc_path
    nvmrc_path="$(nvm_find_nvmrc)"

    if [ -n "$nvmrc_path" ]; then
      local nvmrc_node_version
      nvmrc_node_version=$(nvm version "$(cat "${nvmrc_path}")")

      if [ "$nvmrc_node_version" = "N/A" ]; then
        nvm install
      elif [ "$nvmrc_node_version" != "$(nvm version)" ]; then
        nvm use
      fi
    elif [ -n "$(PWD=$OLDPWD nvm_find_nvmrc)" ] && [ "$(nvm version)" != "$(nvm version default)" ]; then
      echo "Reverting to nvm default version"
      nvm use default
    fi
  }

  add-zsh-hook chpwd load-nvmrc
  load-nvmrc
fi

# sdkman setup
#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$XDG_DATA_HOME/sdkman"
[[ -s "$SDKMAN_DIR/bin/sdkman-init.sh" ]] && source "$SDKMAN_DIR/bin/sdkman-init.sh"


" turn on wrap
setlocal wrap
" do not break at the middle of a word
setlocal linebreak
" turn off list mode
setlocal nolist
" set break symbal
setlocal showbreak=…
" spell check
setlocal spell
setlocal spell spelllang=en_us
setlocal spellfile=~/.local/share/spellfile.add

if !exists("g:vim_markdown_preview_temp_file")
  let g:vim_markdown_preview_temp_file = 0
endif

function! Vim_Markdown_Preview()
  let b:curr_file = expand('%:p')

  call system('pandoc --css ~/.config/pandoc/markdown-preview.css --standalone --mathjax --title vim-markdown-preview.html "' . b:curr_file . '" > /tmp/vim-markdown-preview.html')
  if v:shell_error
    echo 'Pandoc is required to preview markdown!'
  endif

  let browser_wid = system("xdotool search --name 'vim-markdown-preview.html'")
  if !browser_wid
    call system('firefox --new-window /tmp/vim-markdown-preview.html 1>/dev/null 2>/dev/null &')
  else
    " remove the new line at the end of the window id
    let browser_wid = substitute(browser_wid, '\n\+$', '', '')
    call system('xdotool key --window ' . browser_wid . ' "ctrl+r"')
  endif

  if g:vim_markdown_preview_temp_file == 1
    sleep 200m
    call system('rm /tmp/vim-markdown-preview.html')
  endif
endfunction
command! PreviewMarkdown call Vim_Markdown_Preview()

nmap <silent> <leader>p :PreviewMarkdown<CR>

" change bold text color
hi VimwikiBold cterm=bold ctermfg=White guifg=#FFFFFF gui=bold
hi VimwikiItalic cterm=italic ctermfg=White guifg=#FFFFFF gui=italic

" AutoPairs - custom pair settings for markdown
let b:AutoPairs = {'(':')', '[':']', '{':'}', "'":"'", '"':'"', '*':'*', "`":"`", '```':'```', '"""':'"""', "'''":"'''"}

" surround: use shortcut '-' to add double *
let b:surround_45 = "**\r**"

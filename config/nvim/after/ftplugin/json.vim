" Turn on auto indent
setlocal autoindent

" Turn on folding
setlocal foldenable

" Set folding method
setlocal foldmethod=indent

" Format json using jq
nmap <silent> <leader>jq :%! jq .<CR>

